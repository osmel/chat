//Librerías necesarias
//var app     =     require("express")();
//var mysql   =     require("mysql");
//var http    =     require('http').Server(app);
//var io      =     require("socket.io")(http); 
//var io = require('socket.io').listen(5000);


var express = require('express'),
    http = require('http');
    var mysql   =     require("mysql");
var app = express();
var server = http.createServer(app);
var io = require('socket.io').listen(server);

server.listen(8080);


// Conectando a mysql
var con    =    mysql.createPool({
      connectionLimit   :   100,
      host              :   'localhost',
      user              :   'root',
      password          :   'root',
      database          :   'chat',
      debug             :   false
});

var nombre ='';

/*
app.get("/",function(req,res){
  console.log(res);
    res.sendFile(__dirname + '/index.html');
});
*/


//http://qnimate.com/facebook-style-chat-box-popup-using-javascript-and-css/

//http://markshust.com/2013/11/07/creating-nodejs-server-client-socket-io-mysql
//https://www.npmjs.com/package/mysql
//https://socket.io/docs/rooms-and-namespaces/
//https://www.uno-de-piera.com/chat-con-node-js-express-y-socketio/

//https://codeforgeek.com/2015/03/real-time-app-socket-io/
//https://www.npmjs.com/package/mysql
// Log any errors connected to the db
       

/*
app.get("/",function(req, res){
    res.sendFile(__dirname + '/index.html'); //Ruta para lanzar el index
});*/

io.on('connection',function(socket){  
	socket.emit('conexion', { message: 'Soy el servidor, Que quieres??' });
	//console.log('socket');
	//console.log("1 Usuario esta conectado");

	socket.on('unirSala', function(sessionId,id_user){
		var hab;

        hab = 2;
           con.getConnection(function(err,connection){
                    //console.log(connection);
                if (err) console.log(err);

                con.query("SELECT * FROM  usuarios where id='"+id_user+"'",function(err,filas, campos){
                      
                      connection.release();
                      if(!err) {
                      //  callback(true);
                      }

                      //nombre = filas[0].nombre;
                      //si es administrador "se unira a la habitación admin"
                      if (filas[0].perfil==1)  {
                         socket.join('admin');
                      } 

                      //se unirá a la habitación 2
                      socket.join(hab.toString());
                      //se unira a la habitación de su usuario
                      socket.join(id_user.toString());  
                      
                  });
          }); 

	});	




    
    socket.on('msg_publico',function(msg,id_user,destino){

       con.getConnection(function(err,connection){
                if (err) console.log(err);
                con.query("SELECT * FROM  usuarios where id='"+id_user+"'",function(err,filas, campos){
                      connection.release();
                      if(!err) {
                      //  callback(true);
                      }
                      nombre = filas[0].nombre;
                       var valores = {msg: msg, id_user: id_user, status:'p',  destino:destino, nombre:nombre};
                       var hab=2;
                       addComentario(valores,function(res){
                        if(res){
                            socket.emit('chat_publico',valores);  //para el usuario actual
                            socket.broadcast.to(hab.toString()).emit('chat_publico',valores); //para el resto que esta en hab.
                        } else {
                            io.emit('error');
                        }
                      });
   
                  });






          }); 


     
       
    });


    




    socket.on('msg_privado',function(msg,id_user,destino){

           con.getConnection(function(err,connection){
                if (err) console.log(err);
                con.query("SELECT * FROM  usuarios where id='"+id_user+"'",function(err,filas, campos){
                      connection.release();
                      if(!err) {
                      //  callback(true);
                      }
                      nombre = filas[0].nombre;


                 
                      var valores = {msg: msg, id_user: id_user, status:'v',destino:destino, nombre:nombre};
                       
                      addComentario(valores,function(res){
                        if(res){
                            socket.emit('chat_privado',valores);  //para el usuario actual
                            socket.broadcast.to(destino.toString()).emit('chat_privado',valores); //para el resto que esta en hab.
                        } else {
                            io.emit('error');
                        }
                      });




                  });






          }); 


     
       
    });


    
});


var addComentario = function (valores,callback) {
  //console.log(valores.msg);
        con.getConnection(function(err,connection){
            if (err) {
              //console.log(valores.msg);
              connection.release();
              callback(false);
              return;
            }
       
        connection.query("INSERT INTO mensajes (mensaje,id_usuario, status, id_destino) VALUES ('"+valores.msg+"'"+",'"+valores.id_user+"'"+",'"+valores.status+"'"+",'"+valores.destino+"')", function(err,rows){ //Insertando nuestro comentario
                connection.release();
                //console.log(valores.msg);
                if(!err) {
                  callback(true);
                }
            });
         connection.on('error', function(err) {
                  callback(false);
                  return;
            });
        });
}

/*
http.listen(5000,function(){ // Puesta en marcha del servidor en el puerto 5000
    console.log("Listening on 5000"); 
});
*/